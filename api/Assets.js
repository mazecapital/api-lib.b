"use strict";

const BaseEntity = require('./BaseEntity');

class Assets extends BaseEntity {
  constructor(...props) {
    super(...props);

    this.slug = 'assets';
  }
}

module.exports = new Assets();
