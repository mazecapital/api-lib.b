"use strict";

class Trade {
  constructor(market_id, price, amount, time, time_received) {
    this.market_id = parseInt(market_id);
    this.price = parseFloat(price);
    this.amount = parseFloat(amount);
    this.time = new Date(parseInt(time));
    this.time_received = new Date(parseInt(time_received));
  }

  toJSON() {
    return {
      market_id: this.market_id,
      price: this.price,
      amount: this.amount,
      time: this.time.getTime(),
      time_received: this.time_received.getTime(),
    };
  }
}

module.exports = Trade;
