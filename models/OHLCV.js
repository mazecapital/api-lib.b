"use strict";

class OHLCV {
  constructor(market_id, open, high, low, close, volume, timeframe, time, time_received) {
    this.market_id = parseInt(market_id);
    this.open = parseFloat(open);
    this.high = parseFloat(high);
    this.low = parseFloat(low);
    this.close = parseFloat(close);
    this.volume = parseFloat(volume);
    this.timeframe = parseInt(timeframe);
    this.time = new Date(parseInt(time));
    this.time_received = new Date(parseInt(time_received));
  }

  toJSON() {
    return {
      market_id: this.market_id,
      open: this.open,
      high: this.high,
      low: this.low,
      close: this.close,
      volume: this.volume,
      timeframe: this.timeframe,
      time: this.time.getTime(),
      time_received: this.time_received.getTime(),
    };
  }
}

module.exports = OHLCV;
