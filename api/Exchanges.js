"use strict";

const BaseEntity = require('./BaseEntity');

class Exchanges extends BaseEntity {
  constructor(...props) {
    super(...props);

    this.slug = 'exchanges';
  }
}

module.exports = new Exchanges();
