"use strict";

const axios = require('axios');

const API_TIMEOUT = 2000;

const Api = axios.create({
  baseURL: process.env.MADOFF_API_URL || process.env.VUE_APP_MADOFF_API_URL || 'http://api:1337', // TODO: This should not be 'http://api:1337' if env is unset but the prod api url
  timeout: API_TIMEOUT,
});

// if (localStorage.userJwt) {
//   Api.defaults.headers.common.Authorization = `Bearer ${localStorage.userJwt}`;
// }

module.exports = Api;
