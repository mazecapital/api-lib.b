"use strict";

const Api = require('./Api');

class OHLCVs {
  constructor() {
    this.slug = 'ohlcvs';
  }

  find(params) { return Api.get(`${this.slug}`, { params }) }
}

module.exports = new OHLCVs();
