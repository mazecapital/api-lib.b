"use strict";

const Api = require('./Api');

class BaseEntity {
  constructor() {
    this.slug = null;
  }

  find(params) { return Api.get(`${this.slug}`, { params }) }
  count(params) { return Api.get(`${this.slug}/count`, { params }) }
  get(id) { return Api.get(`${this.slug}/${id}`) }
  insert(entity) { return Api.post(`${this.slug}`, entity) }
  update(entity) { return Api.put(`${this.slug}/${entity.id}`, entity) }
  save(entity) { return (entity.id ? this.update(entity) : this.insert(entity)) }
  delete(entity) { return Api.delete(`${this.slug}/${entity.id}`) }
}

module.exports = BaseEntity;
