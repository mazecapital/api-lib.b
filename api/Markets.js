"use strict";

const BaseEntity = require('./BaseEntity');

class Markets extends BaseEntity {
  constructor(...props) {
    super(...props);

    this.slug = 'markets';
  }
}

module.exports = new Markets();
